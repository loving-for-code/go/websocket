module websocket-app

go 1.20

require golang.org/x/net v0.22.0 // indirect

require (
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.1
)
