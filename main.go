package main

import (
	"context"
	"log"
	"net/http"
)

// type Server struct {
// 	cons map[*websocket.Conn]bool
// }

// func NewServer() *Server {
// 	return &Server{
// 		cons: make(map[*websocket.Conn]bool),
// 	}
// }

// func (s *Server) handleWS(ws *websocket.Conn) {
// 	fmt.Println("new incoming connection from client:", ws.RemoteAddr())
// 	s.cons[ws] = true

// 	s.readLoop(ws)
// }

// func (s *Server) readLoop(ws *websocket.Conn) {
// 	buf := make([]byte, 1024)
// 	for {
// 		n, err := ws.Read(buf)
// 		if err != nil {
// 			// delete(s.cons, ws)
// 			// ws.Close()
// 			// break
// 			if err == io.EOF {
// 				break
// 			}
// 			fmt.Println("read error:", err)
// 			return
// 		}

// 		msg := buf[:n]

// 		s.broadcast(msg)
// 	}

// }

// func (s *Server) broadcast(b []byte) {
// 	for ws := range s.cons {
// 		go func(ws *websocket.Conn) {
// 			if _, err := ws.Write(b); err != nil {
// 				fmt.Println("write error:", err)
// 			}
// 		}(ws)
// 	}
// }

// func main() {
// 	server := NewServer()
// 	http.Handle("/ws", websocket.Handler(server.handleWS))
// 	http.ListenAndServe(":3000", nil)
// }

func main() {
	setupAPI()

	log.Fatal(http.ListenAndServeTLS(":3000", "server.crt", "server.key", nil))
}

func setupAPI() {
	ctx := context.Background()

	manager := NewManager(ctx)

	http.Handle("/", http.FileServer(http.Dir("./fe")))
	http.HandleFunc("/ws", manager.serveWS)
	http.HandleFunc("/login", manager.loginHandler)
}
