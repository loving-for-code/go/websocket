package main

import (
	"encoding/json"
	"log"
	"time"

	"github.com/gorilla/websocket"
)

var (
	pongWait     = 10 * time.Second
	pingInterval = (pongWait * 9) / 10
)

type Client struct {
	conn    *websocket.Conn
	manager *Manager

	chatroom string
	// egress is used to avoid concurrent writes on the websocket connection
	egress chan Event
}

type ClientList map[*Client]bool

func NewClient(conn *websocket.Conn, manager *Manager) *Client {
	return &Client{
		conn:    conn,
		manager: manager,
		egress:  make(chan Event),
	}
}

func (c *Client) readMessage() {
	defer func() {
		// close connection
		c.manager.removeClient(c)
	}()

	if err := c.conn.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
		log.Println("error setting read deadline: ", err)
		return
	}

	c.conn.SetReadLimit(512)

	c.conn.SetPongHandler(c.pongHandler)

	for {
		_, payload, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		// for wsclient := range c.manager.clients {
		// 	wsclient.egress <- payload
		// }

		// log.Println("received message:", string(payload))
		// log.Println("message type:", messageType)

		var request Event
		if err := json.Unmarshal(payload, &request); err != nil {
			log.Println("error unmarshalling message: ", err)
			break
		}

		if err := c.manager.routeEvent(request, c); err != nil {
			log.Println("error routing message: ", err)
			break
		}
	}
}

func (c *Client) writeMessage() {
	defer func() {
		// close connection
		c.manager.removeClient(c)
	}()

	ticker := time.NewTicker(pingInterval)

	for {
		select {
		case message, ok := <-c.egress:
			if !ok {
				if err := c.conn.WriteMessage(websocket.CloseMessage, []byte{}); err != nil {
					log.Println("error writing close message:", err)
				}
				return
			}

			data, err := json.Marshal(message)
			if err != nil {
				log.Println("error marshalling message:", err)
				return
			}

			if err := c.conn.WriteMessage(websocket.TextMessage, data); err != nil {
				return
			}
			log.Println("sent message:", string(data))
		case <-ticker.C:
			log.Println("ping")

			// Send a Ping to the Client
			if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				log.Println("error writing ping:", err)
				return
			}
		}
	}
}

func (c *Client) pongHandler(pongMsg string) error {
	log.Println("pong")
	return c.conn.SetReadDeadline(time.Now().Add(pongWait))
}
